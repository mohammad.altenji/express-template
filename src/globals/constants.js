import path from 'path';
import config from 'config';

export const responseStatuses = {
  success: 'success',
  fail: 'failure'
};

export const errorMessages = {
  notFound: 'Page not found',
  activePlanAlert: 'There is already an active plan!',
  genericError: 'GENERIC_ERROR',
  planNotFound: 'Plan not found!',
  fullScrumPlan: 'This scrum plan is already full with voters!',
  storyAlreadyVoted: 'There is already givven point for this story!',
  onlyActiveStoryCanBeVoted: 'Only active story can be voted!',
  teamVotesAreIncompleted:
    'You can not vote the final story point now, Team votes are not completed yet!',
  allStoriesAreVoted: 'You can not vote any more, All stories are voted!',
  planIsFinished: 'This scrum poker session is finished!'
};

export const viewsPath = path.join(path.resolve('.'), config.PUBLIC_VIEW_PATH);

export const oneHour = 60 * 60;

export const oneDay = oneHour * 24;

export const storyStatus = {
  ACTIVE: 'active',
  NOT_VOTED: 'not-voted',
  VOTED: 'voted'
};

export const voterStatus = {
  NOT_VOTED: 'not-voted',
  VOTED: 'voted'
};
