import MD5 from 'crypto-js/md5';

export const checkValue = (value, key) => {
  value = key
    ? key
        .split('.')
        .reduce(
          (o, x) => (typeof o == 'undefined' || o === null ? o : o[x]),
          value
        )
    : value;
  return (!value && value !== false) || /^\s*$/.test(value) ? false : true;
};

export const sendClientAlarm = msg => {
  return `
	  <div 
		style="
		  width: 50%;
		  height: auto;
		  margin: auto;
		  text-align: center;
		  padding: 20px;
		  margin-top: 70px;
		  font-family: sans-serif !important;
		  background-color: #9e5050;
		  color: #fff;">
		<h3>${
      msg
        ? `${msg} <br /> you can create new scrum plan from this <a href="/scrum-plan-create">link</a>`
        : `Generic error happened, please contact the admin!`
    }</h3>
	  </div>
	`;
};

export const handleMissedParamsError = errors =>
  `${errors.map(param => `<br />-[${param}] should not be empty`)}`;

export const createHash = (arr, key) => {
  var Hash = function() {
    this.data = {};
    if (arr && Array.isArray(arr)) {
      arr.map(o => {
        this.data[o[key]] = o;
      });
      this.size = arr.length;
    } else {
      this.size = 0;
    }
  };
  Hash.prototype.keys = function() {
    return Object.keys(this.data);
  };
  Hash.prototype.values = function() {
    return Object.values(this.data);
  };

  return new Hash();
};

export const generateRandomId = message => MD5(message).toString() + Date.now();

export const getFibArr = n => {
  var i;
  var fib = [0, 1];
  for (i = 2; i <= n; i++) {
    fib[i] = fib[i - 2] + fib[i - 1];
  }
  fib.splice(0, 2);
  return fib;
};
