var socket = io();

function App() {}

App.activeStory = {};
App.scrumPlan = {};
App.currentVoter = {};

App.prototype.showErrorAlert = function(errorMessage) {
  $('#failure-message').html(errorMessage || 'some error happened!');
  $('#failure-message').removeClass('hidden');
  setTimeout(function() {
    $('#failure-message').addClass('hidden');
  }, 10000);
};

App.prototype.startPlanSession = function(payload) {
  $.ajax({
    type: 'POST',
    url: '/scrum-plan/create',
    contentType: 'application/json',
    data: JSON.stringify(payload),
    success: data => {
      window.location.href = `/scrum-plan-manage/${data.data.planId}`;
    },
    error: err => {
      this.showErrorAlert(JSON.parse(err.responseText).errorMessage);
    }
  });
};

App.prototype.voteStoryPoint = function(payload, elm) {
  $.ajax({
    type: 'POST',
    url: '/scrum-plan/story/vote',
    contentType: 'application/json',
    data: JSON.stringify(payload),
    success: data => {
      $('.fib-item').removeClass('selected');
      elm.addClass('selected');
      $('#current-vote-value').text(
        `${payload.voteValue} Voted!`
      );
    },
    error: err => {
      this.showErrorAlert(JSON.parse(err.responseText).errorMessage);
    }
  });
};

App.prototype.submitFinalStoryPoint = function(payload) {
  $.ajax({
    type: 'POST',
    url: '/scrum-plan/story/final-vote',
    contentType: 'application/json',
    data: JSON.stringify(payload),
    success: data => {
      $('#scrum-master-vote').val('');
    },
    error: err => {
      this.showErrorAlert(JSON.parse(err.responseText).errorMessage);
    }
  });
};

var app = new App();
