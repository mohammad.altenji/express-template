import socketIO from 'socket.io';
import { eventHandler, emittedEvents } from 'IOEvents';

class Socket {
  constructor() {
    if (!!Socket.instance) {
      return Socket.instance;
    }
    Socket.instance = this;
  }

  connect(server) {
    var io = socketIO.listen(server);

    io.on('connection', function (socket) {
      console.log("Connected succesfully to the socket ...");

      Object.values(emittedEvents).forEach(emittedEvent => {
        eventHandler.on(emittedEvent, (data) => {
          socket.emit(emittedEvent, data);
        });
      });
    });
  }
};

export default new Socket();
