import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import http from 'http';
import path from 'path';
import hbs from 'hbs';
import cors from 'cors';

import api from 'api';

import config from 'config';
import { apiExistParamsValidation } from 'middlewares';

class Express {
  constructor() {
    if (!!Express.instance) {
      return Express.instance;
    }
    Express.instance = this;

    this.app = express();
    this.server = http.createServer(this.app);
    this.app.set('view engine', 'html');
    this.app.engine('html', hbs.__express);
    if (!config.IS_PROD) {
      this.app.use(cors());
    }
    this.app.use(
      express.static(path.join(path.resolve('.'), config.PUBLIC_VIEW_PATH))
    );
    this.app.use(express.static(config.PUBLIC_STATIC_PATH));
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());

    this.app.use(session({
      secret: config.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: {
        expires: false,
        maxAge: 86400 * 150 * 1000, // means after one month
      }
    }));

    this.app.use('/', apiExistParamsValidation);

    this.app.use(api());

    // global error handling middleware
    this.app.use((err, req, res, next) => {
      console.log('err is: ', err); // to see properties of message in our console
      res.status(422).send(err.message ? err.message : err);
    });
  }

  connect(port) {
    this.port = port || config.PORT;

    this.server.listen(this.port, config.HOSTNAME, () => {
      console.log(`listening on port ${this.port}`);
    });
  }
};

export default new Express();
