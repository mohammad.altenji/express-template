import NodeCache from 'node-cache';

import { oneDay } from 'globals/constants';

class CasheMemory {
  constructor() {
    if (!!CasheMemory.instance) {
      return CasheMemory.instance;
    }
    CasheMemory.instance = this;

    this.myCache = new NodeCache({
      stdTTL: oneDay
    });
  }
}

const myCache = new CasheMemory().myCache;
export default myCache;
