import expressServer from './Express';
import socketIO from './Socket';
import hbsViews from './HbsViews';

async function loadServer() {
  expressServer.connect();
  socketIO.connect(expressServer.server);
  hbsViews.initialize();
}

export default loadServer;