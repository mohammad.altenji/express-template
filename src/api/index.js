import { Router } from 'express';

import viewsRouter from './viewsRouter';
import scrumPlanRouter from './scrumPlanRouter';

export default () => {
	const router = Router();
	
	viewsRouter(router);
	scrumPlanRouter(router);
	
	return router;
}