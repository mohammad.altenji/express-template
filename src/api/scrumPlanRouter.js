import routes from 'config/routes';
import {
  ScrumPlanCreateService,
  ScrumPlanStoryVoteService,
  ScrumPlanStoryFinalVoteService
} from 'services';
import {
  scrumPlanCreateMiddleware,
  scrumPlanStoryVoteMiddleware,
  scrumPlanStoryFinalVoteMiddleware
} from 'middlewares';
import { serviceHandler } from 'utils/service';

const scrumPlanRouter = router => {
  router.post(routes.scrumPlanCreate.url, (req, res, next) => {
    serviceHandler({
      req,
      res,
      next,
      middleware: scrumPlanCreateMiddleware,
      service: ScrumPlanCreateService
    });
  });

  router.post(routes.scrumPlanCreate.url, (req, res, next) => {
    serviceHandler({
      req,
      res,
      next,
      middleware: scrumPlanStoryVoteMiddleware,
      service: ScrumPlanStoryVoteService
    });
  });

  router.post(routes.ScrumPlanStoryVote.url, (req, res, next) => {
    serviceHandler({
      req,
      res,
      next,
      middleware: scrumPlanStoryVoteMiddleware,
      service: ScrumPlanStoryVoteService
    });
  });

  router.post(routes.scrumPlanStoryFinalVote.url, (req, res, next) => {
    serviceHandler({
      req,
      res,
      next,
      middleware: scrumPlanStoryFinalVoteMiddleware,
      service: ScrumPlanStoryFinalVoteService
    });
  });
};

export default scrumPlanRouter;
