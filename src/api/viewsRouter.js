import routes from 'config/routes';
import {
  ScrumManageViewService,
  ScrumPlanDeveloperViewService
} from 'services';
import {
  scrumPlanManageViewMiddleware,
  scrumPlanDeveloperViewMiddleware
} from 'middlewares';
import { viewServiceHandler } from 'utils/service';

const viewsRouter = router => {
  router.get(routes.homeView.url, async (req, res, next) => {
    await viewServiceHandler({
      req,
      res,
      next,
      viewPathUrl: routes.homeView.path,
    });
  });

  router.get(routes.scrumCreateView.url, async (req, res, next) => {
    await viewServiceHandler({
      req,
      res,
      next,
      viewPathUrl: routes.scrumCreateView.path,
    });
  });

  router.get(routes.scrumManageView.url, async (req, res, next) => {
    await viewServiceHandler({
      req,
      res,
      next,
      viewPathUrl: routes.scrumManageView.path,
      middleware: scrumPlanManageViewMiddleware,
      service: ScrumManageViewService
    });
  });

  router.get(routes.scrumPlanDeveloperView.url, async (req, res, next) => {
    await viewServiceHandler({
      req,
      res,
      next,
      viewPathUrl: routes.scrumPlanDeveloperView.path,
      middleware: scrumPlanDeveloperViewMiddleware,
      service: ScrumPlanDeveloperViewService
    });
  });
};

export default viewsRouter;
