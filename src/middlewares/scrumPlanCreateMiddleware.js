import myCache from 'loaders/CasheMemory';
import { errorMessages } from 'globals/constants';
import { ScrumPlanGetService } from 'services';

const scrumPlanCreateMiddleware = async req => {
  const scrumPlan = ScrumPlanGetService();

  if (scrumPlan && !!scrumPlan.planStatus && !req.body.overwritePlan) {
    throw {
      errorMessage: errorMessages.activePlanAlert
    };
  }
};

export default scrumPlanCreateMiddleware;
