import { ScrumPlanGetService } from 'services';
import { errorMessages } from 'globals/constants';

const scrumPlanStoryVoteMiddleware = async (req, next) => {
  const scrumPlan = ScrumPlanGetService();
  const { voterId } = req.body;
  const selectedVoter = scrumPlan.voterList.find(
    voter => voter.voterId === voterId
  );
  const allStoriesAreVoted = scrumPlan.storyList.every(story => !!story.storyPoint);

  if (allStoriesAreVoted) {
    throw {
      errorMessage: errorMessages.allStoriesAreVoted
    };
  }
  if (!scrumPlan || !selectedVoter) {
    throw {
      errorMessage: errorMessages.genericError
    };
  }
};

export default scrumPlanStoryVoteMiddleware;
