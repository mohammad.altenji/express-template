import { errorMessages } from 'globals/constants';
import { ScrumPlanGetService } from 'services';

const scrumPlanDeveloperViewMiddleware = async req => {
  const scrumPlan = ScrumPlanGetService();

  if (!scrumPlan || scrumPlan.planId !== req.params.planId || !scrumPlan.planStatus) {
    throw {
      errorMessage: errorMessages.planNotFound,
    };
  }
  const isAllVotersAreUnavailable = scrumPlan.voterList.every(voter => voter.isUsed);
  if(!req.session.userVoterId && isAllVotersAreUnavailable) {
    throw {
      errorMessage: errorMessages.fullScrumPlan,
    };
  }

};

export default scrumPlanDeveloperViewMiddleware;
