import { ScrumPlanGetService } from 'services';
import { errorMessages, storyStatus } from 'globals/constants';

const ScrumPlanStoryFinalVoteService = async (req, next) => {
  const scrumPlan = ScrumPlanGetService();
  const { storyId } = req.body;
  const selectedStory = scrumPlan.storyList.find(
    story => story.storyId === storyId
  );
  const isAllVotersHaveVoteValue = scrumPlan.voterList.every(
    voter => !!voter.voteValue
  );
  const allStoriesAreVoted = scrumPlan.storyList.every(story => !!story.storyPoint);

  if (allStoriesAreVoted) {
    throw {
      errorMessage: errorMessages.allStoriesAreVoted
    };
  }
  if (!scrumPlan || !selectedStory) {
    throw {
      errorMessage: errorMessages.genericError
    };
  }
  if (!isAllVotersHaveVoteValue) {
    throw {
      errorMessage: errorMessages.teamVotesAreIncompleted
    };
  }
  if (selectedStory.storyStatus !== storyStatus.ACTIVE) {
    throw {
      errorMessage: errorMessages.onlyActiveStoryCanBeVoted
    };
  }
  if (!!selectedStory.storyPoint) {
    throw {
      errorMessage: errorMessages.storyAlreadyVoted
    };
  }
};

export default ScrumPlanStoryFinalVoteService;
