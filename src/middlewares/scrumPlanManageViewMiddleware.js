import { ScrumPlanGetService, ScrumPlanSetService } from 'services';
import { errorMessages } from 'globals/constants';

const scrumPlanManageViewMiddleware = async (req, next) => {
  const scrumPlan = ScrumPlanGetService();

  if (!scrumPlan || scrumPlan.planId !== req.params.planId || !scrumPlan.planStatus) {
    throw {
      errorMessage: errorMessages.planNotFound,
    };
  }

  const allStoriesAreVoted = scrumPlan.storyList.every(story => !!story.storyPoint);
  if(allStoriesAreVoted) {
    ScrumPlanSetService({});
    throw {
      errorMessage: errorMessages.planIsFinished,
    };
  }
};

export default scrumPlanManageViewMiddleware;
