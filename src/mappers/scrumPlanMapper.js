import mapVoters from './voterMappper';
import mapStories from './storyMappper';
import { generateRandomId } from 'globals/helpers';

const mapScrumPlan = ({
  numberOfVoters,
  planName,
  storyList
}) => ({
  numberOfVoters,
  planName,
  storyList: mapStories(storyList),
  planStatus: 1,
  voterList: mapVoters(numberOfVoters),
  planId: generateRandomId('planId'),
});

export default mapScrumPlan;