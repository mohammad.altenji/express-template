import { generateRandomId } from 'globals/helpers';
import { voterStatus } from 'globals/constants';

const mapVoters = numberOfVoters => {
  const emptyVotersArr = Array(Number(numberOfVoters) + 1).fill();
  return emptyVotersArr.map((val, index) => ({
    voterName:
      index === emptyVotersArr.length - 1
        ? 'Scrum master'
        : `Voter ${index + 1}`,
    votingStatus: voterStatus.NOT_VOTED,
    isScrumPlanner: index === emptyVotersArr.length - 1,
    isUsed: index === emptyVotersArr.length - 1,
    voteValue: null,
    voterId: generateRandomId(`voterId${index}`)
  }));
};

export default mapVoters;
