import { generateRandomId } from 'globals/helpers';
import { storyStatus } from 'globals/constants';

const mapStories = storyList => {
  return storyList.map((val, index) => ({
    storyName: val,
    storyStatus: index === 0 ? storyStatus.ACTIVE : storyStatus.NOT_VOTED,
    storyPoint: null,
    storyId: generateRandomId(`storyId${index}`)
  }));
};

export default mapStories;
