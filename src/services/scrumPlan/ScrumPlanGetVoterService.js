import { ScrumPlanGetService, ScrumPlanSetService } from 'services';

const ScrumPlanGetVoterService = req => {
  const scrumPlan = ScrumPlanGetService(req);
  const { userVoterId } = req.session;
  let currentVoter;
  const sessionVoterIndex = scrumPlan.voterList.findIndex(
    voter => voter.voterId === userVoterId
  );
  if (sessionVoterIndex !== -1) {
    currentVoter = scrumPlan.voterList[sessionVoterIndex];
    scrumPlan.voterList[sessionVoterIndex].isUsed = true;
  } else {
    currentVoter = scrumPlan.voterList.find(
      voter => !voter.isUsed
    );
    req.session.userVoterId = currentVoter.voterId;

    const currentVoterIndex = scrumPlan.voterList.findIndex(
      voter => voter.voterId === currentVoter.voterId
    );
    scrumPlan.voterList[currentVoterIndex].isUsed = true;
  }

  ScrumPlanSetService(scrumPlan);

  return currentVoter;
};

export default ScrumPlanGetVoterService;
