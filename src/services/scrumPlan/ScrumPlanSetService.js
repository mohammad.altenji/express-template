import myCache from 'loaders/CasheMemory';

const ScrumPlanSetService = scrumPlanData => {
  myCache.set('plan', scrumPlanData)
};

export default ScrumPlanSetService;