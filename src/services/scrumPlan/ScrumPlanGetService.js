import myCache from 'loaders/CasheMemory';

const ScrumPlanGetService = req => {
  const scrumPlan = myCache.get('plan');

  return scrumPlan;
};

export default ScrumPlanGetService;
