import { ScrumPlanGetService, ScrumPlanSetService } from 'services';
import { voterStatus } from 'globals/constants';
import { eventHandler, emittedEvents } from 'IOEvents';

const ScrumPlanStoryVoteService = req => {
  const scrumPlan = ScrumPlanGetService();
  const { voteValue, voterId } = req.body;
  const selectedVoterIndex = scrumPlan.voterList.findIndex(
    voter => voter.voterId === voterId
  );

  scrumPlan.voterList[selectedVoterIndex].voteValue = voteValue;
  scrumPlan.voterList[selectedVoterIndex].votingStatus = voterStatus.VOTED;
  ScrumPlanSetService(scrumPlan);

  eventHandler.emit(emittedEvents.votingChanged, {
    voterList: scrumPlan.voterList
  });

  return { currentVoter: scrumPlan.voterList[selectedVoterIndex] };
};

export default ScrumPlanStoryVoteService;
