import { ScrumPlanGetService, ScrumPlanSetService } from 'services';
import { storyStatus, voterStatus } from 'globals/constants';
import { eventHandler, emittedEvents } from 'IOEvents';

const ScrumPlanStoryFinalVoteService = req => {
  const scrumPlan = ScrumPlanGetService();
  const { storyId, finalVoteValue } = req.body;
  const selectedStoryIndex = scrumPlan.storyList.findIndex(
    story => story.storyId === storyId
  );

  scrumPlan.storyList[selectedStoryIndex].storyPoint = finalVoteValue;
  scrumPlan.storyList[selectedStoryIndex].storyStatus = storyStatus.VOTED;

  const nextStory = scrumPlan.storyList[selectedStoryIndex + 1];
  if (nextStory && nextStory.storyStatus === storyStatus.NOT_VOTED) {
    scrumPlan.storyList[selectedStoryIndex + 1].storyStatus =
      storyStatus.ACTIVE;
  }

  scrumPlan.voterList = scrumPlan.voterList.map(voter => ({
    ...voter,
    votingStatus: voterStatus.NOT_VOTED,
    voteValue: null,
  }));
  
  ScrumPlanSetService(scrumPlan);

  eventHandler.emit(emittedEvents.storyListUpdated, {
    storyList: scrumPlan.storyList
  });

  eventHandler.emit(emittedEvents.votingChanged, {
    voterList: scrumPlan.voterList
  });

  return { activeStory: scrumPlan.storyList[selectedStoryIndex] };
};

export default ScrumPlanStoryFinalVoteService;
