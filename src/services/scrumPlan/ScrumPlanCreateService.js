
import mapScrumPlan from 'mappers/scrumPlanMapper';
import { ScrumPlanGetService, ScrumPlanSetService } from 'services';

const ScrumPlanCreateService =  async req => {
  const planObj = mapScrumPlan(req.body);
  ScrumPlanSetService(planObj);
  const scrumPlan = ScrumPlanGetService();

  return ({
    planId: planObj.planId,
    scrumPlan: scrumPlan
  })
}

export default ScrumPlanCreateService;