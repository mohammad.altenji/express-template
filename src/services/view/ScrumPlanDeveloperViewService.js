import { ScrumPlanGetService, ScrumPlanGetVoterService } from 'services';
import { getFibArr } from 'globals/helpers';
import { storyStatus } from 'globals/constants';
import myCache from 'loaders/CasheMemory';

const ScrumPlanDeveloperViewService = async req => {
  const currentVoter = ScrumPlanGetVoterService(req);

  const scrumPlan = ScrumPlanGetService(req);
  const activeStory = scrumPlan.storyList.find(
    story => story.storyStatus === storyStatus.ACTIVE
  );
  const fibArr = getFibArr(12).map(val => ({
    value: val
  }));

  return {
    currentVoter,
    scrumPlan,
    activeStory,
    fibArr,
  };
};

export default ScrumPlanDeveloperViewService;
