import { ScrumPlanGetService } from 'services';
import { getFibArr } from 'globals/helpers';
import { storyStatus } from 'globals/constants';

const ScrumManageViewService = async req => {
  const scrumPlan = ScrumPlanGetService(req);
  const activeStory = scrumPlan.storyList.find(
    story => story.storyStatus === storyStatus.ACTIVE
  );
  const fibArr = getFibArr(12).map(val => ({
    value: val
  }));
  const currentVoter = scrumPlan.voterList.find((voter) => voter.isScrumPlanner);
  const planLink = `${req.protocol}://${req.headers.host}/scrum-plan/${scrumPlan.planId}`;

  return {
    scrumPlan,
    fibArr,
    activeStory,
    currentVoter,
    planLink
  };
};

export default ScrumManageViewService;
