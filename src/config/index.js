require('dotenv').config();

export default {
	PORT: process.env.PORT || '8080',
	HOSTNAME: process.env.HOSTNAME || '',
	IS_PROD: process.env.IS_PROD === 'true',
	PUBLIC_VIEW_PATH: '/src/views/',
	PUBLIC_IMAGES_PATH: '/src/public/images/',
	PUBLIC_THUMBNAIL_IMAGES_PATH: '/src/public/imagesThumbnail/',
	PUBLIC_SMALL_IMAGES_PATH: '/src/public/imagesSmall/',
	PUBLIC_IMAGES_LINK: '/images/',
	PUBLIC_THUMBNAIL_IMAGES_LINK: '/imagesThumbnail/',
	PUBLIC_SMALL_IMAGES_LINK: '/imagesSmall/',
	PUBLIC_STATIC_PATH: 'src/public/',
	SESSION_SECRET: 'SOME_SESSION_SECRET_FOR_SCRUM_POKER',
};