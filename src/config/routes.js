export default {
  homeView: {
		url: '/',
		path: '/index.hbs',
		type: 'GET',
	},
	scrumCreateView: {
		url: '/scrum-plan-create',
		path: '/scrumCreate.hbs',
		type: 'GET',
	},
	scrumManageView: {
		url: '/scrum-plan-manage/:planId',
		path: '/scrumManage.hbs',
		type: 'GET',
	},
	scrumPlanCreate: {
		url: '/scrum-plan/create',
		type: 'POST',
		bodyParams: ['planName', 'numberOfVoters', 'storyList'],
	},
	ScrumPlanStoryVote: {
		url: '/scrum-plan/story/vote',
		type: 'POST',
		bodyParams: ['voteValue', 'voterId'],
	},
	scrumPlanDeveloperView: {
		url: '/scrum-plan/:planId',
		path: '/scrumDeveloper.hbs',
		type: 'GET',
	},
	scrumPlanStoryFinalVote: {
		url: '/scrum-plan/story/final-vote',
		type: 'POST',
		bodyParams: ['storyId', 'finalVoteValue'],
	},
};
