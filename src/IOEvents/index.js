import eventObj from 'events';

export const eventHandler = new eventObj.EventEmitter();

export const emittedEvents = {
  storyListUpdated: 'STORY_LIST_UPDATED',
  votingChanged: 'VOTING_CHANGED',
}