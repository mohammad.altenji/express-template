import { responseStatuses, errorMessages, viewsPath } from 'globals/constants';
import { sendClientAlarm } from 'globals/helpers';
import logger from 'utils/logger';

export const serviceHandler = async ({
  req,
  res,
  next,
  middleware,
  service
}) => {
  try {
    middleware && (await middleware({ ...req }));
    const serviceResponse = await service({ ...req });
    res.send({
      status: responseStatuses.success,
      data: serviceResponse
    });
  } catch (error) {
    logger.error(error.errorMessage || error.message);
    res.status(error.statusCode || 500).send({
      status: responseStatuses.fail,
      errorMessage:
        error.errorMessage || error.message || errorMessages.genericError
    });
  }
};

export const viewServiceHandler = async ({
  req,
  res,
  next,
  viewPathUrl,
  middleware,
  service
}) => {
  try {
    middleware && (await middleware({ ...req }));
    let response = {};
    if (service) {
      req.fromView = true;
      const serviceData = await service(req);
      response = { ...serviceData };
    }

    res.render(viewsPath + viewPathUrl, { data: response });
  } catch (err) {
    console.log('err: ', err);
    let error = err;
    if (typeof err === 'object' && !err.errorMessage) {
      error = JSON.stringify(err);
    }
    logger.error(error.errorMessage || error.message);
    res.send(
      sendClientAlarm(
        error.errorMessage || error.message || errorMessages.genericError
      )
    );
  }
};
