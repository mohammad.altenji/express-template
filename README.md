## Deployed project URL

https://express-sp.herokuapp.com/

## How to run locally

    npm i
    
    npm start

## How to build

    npm i
    
    npm run build